# Aplicación Gastos

Esta aplicación de escritorio ayuda al usuario a tener un registro de sus gastos personales.
Solo necesita crear un usuario con su cuenta de correo electrónico y una contraseña
Ya al iniciar sesión se podrán encontrar diferentes funciones que ayudan al usuario a
gestionar sus gastos clasificándolos por categorías de forma detallada con nombre del 
gasto, monto del gasto, descripción del gasto y la fecha en la que se realizó.

## Curso

Aplicaciones de Software para Telecomunicaciones

## Herramientas

Las herramientas utilizadas en este proyecto son:

- Javascript
- HTML
- CSS
- Git
- Visual Studio Code
- Node JS
- Electron