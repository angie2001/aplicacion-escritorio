//Librerias
const { app, BrowserWindow, Menu,ipcMain, ipcRenderer} = require("electron");
const url = require("url");
const path = require("path");
 
let ventanaPrincipal;



function cargarHTML(archivo){
    ventanaPrincipal.loadURL(
        url.format({
            pathname: path.join(__dirname,`views/${archivo}.html`),
            protocol:"file",
            slashes:true,
        })
    )
}

function crearNuevaVentana(archivo,nombre){
    //Crear la ventana para editar una categoría
    ventanaNueva = new BrowserWindow({
        width:600,
        height:600,
        title:nombre,
        webPreferences:{
            nodeIntegration:true,
            contextIsolation:false,
        }
    });
    //Elimina el menu de la ventana
    // ventanaNueva.setMenu(null);
    //Carga el archivo html en la ventana
    ventanaNueva.loadURL(
        url.format({
            pathname: path.join(__dirname,`views/${archivo}.html`),
            protocol:"file",
            slashes:true,
        })
    );
    //La ventana principal escucha el evento closed
    ventanaNueva.on("closed",()=>{
    //Libera la memoria reservada para la ventana
        ventanaNueva=null;
    });
}

//Sesión inicidada
let inicioSesion = {email:""};

function cargarInicio(){
    //Crea la ventana principal
    ventanaPrincipal=new BrowserWindow({
        webPreferences:{
            nodeIntegration:true,
            contextIsolation:false,
        }
    });
    //Carga el archivo HTML en la ventana
    ventanaPrincipal.loadURL(
        url.format({
            pathname: path.join(__dirname,"views/index.html"),
            protocol:"file",
            slashes:true,
        })
    );
    //Construye el menú principal
    const menuPrincipal = Menu.buildFromTemplate(templateMenu);
    Menu.setApplicationMenu(menuPrincipal);
    //La ventana principal escucha el evento closed
    ventanaPrincipal.on("closed", ()=>{
        //Cierra la aplicación 
        app.quit();
    });
    
    //EVENTOS GENERALES
    //ipcMain escucha el evento de nuevaCuenta
    ipcMain.on("nuevaCuenta",(evento,datos)=>{
        cargarHTML("index");
    })
    
    
    //ipcMain escucha el evento de inicioLaSesion
    ipcMain.on("inicioLaSesion",(evento,datos)=>{
        inicioSesion.email = datos.email;
        console.log("desde el main f "+ inicioSesion.email);
        //ipcRenderer.send("sesionIniciada",inicioSesion);
        //evento.reply('emailCuenta',datos);
        //evento.returnValue = datos;
        cargarHTML("menu_cuenta");
        ventanaPrincipal.on("ready-to-show", () => {
            /* Despues que ocurre el evento se manda el mensaje*/
            ventanaPrincipal.webContents.send("emailCuenta", datos);
          });
    })


    //EVENTOS CON CATEGORÍAS
    //ipcMain escucha el evento "añadirLaCategoria"
    ipcMain.on("añadirLaCategoria",(evento)=>{
        //Crea una nueva ventana para editar la categoría
        // crearNuevaVentana("crear_categoria","Nueva Categoría")
        cargarHTML("crear_categoria"); 
    })
    
    ipcMain.on("nombreNuevaCategoria",(evento)=>{
        //ventanaPrincipal.webContents.send('nombreNuevaCategoria',datos);
        //
        cargarHTML("menu_cuenta");
    })
    //ipcMain escucha el evento "editarLaCategoria"
    ipcMain.on("editarLaCategoria",(evento,seleccionada)=>{
        //Crea una nueva ventana para editar la categoría
        cargarHTML("editar_categoria");
        ventanaPrincipal.on("ready-to-show", () => {
            /* Despues que ocurre el evento se manda el mensaje*/
            ventanaPrincipal.webContents.send("categoriaSeleccionada", seleccionada);
          });
    })

    ipcMain.on("eliminarLaCategoria",(evento,seleccionada)=>{
        //Crea una nueva ventana para editar la categoría
        cargarHTML("eliminar_categoria");
        ventanaPrincipal.on("ready-to-show", () => {
            /* Despues que ocurre el evento se manda el mensaje*/
            ventanaPrincipal.webContents.send("categoriaSeleccionada", seleccionada);
          });
    })


    //EVENTOS CON GASTOS
    //ipcMain escucha el evento "añadirElGasto"
    ipcMain.on("añadirElGasto",(evento,datos)=>{
        //Crea una nueva ventana para editar el gasto
        // crearNuevaVentana("crear_gasto","Nuevo Gasto")
        cargarHTML("crear_gasto");
    })
    //ipcMain escucha el evento "editarElGasto"
    ipcMain.on("editarElGasto",(evento,seleccionada)=>{
        cargarHTML("editar_gasto");
        ventanaPrincipal.on("ready-to-show", () => {
            /* Despues que ocurre el evento se manda el mensaje*/
            ventanaPrincipal.webContents.send("gastoSeleccionada", seleccionada);
          });
    })
    ipcMain.on("eliminarElGasto",(evento,seleccionada)=>{
        //Crea una nueva ventana para editar la categoría
        cargarHTML("eliminar_gasto");
        ventanaPrincipal.on("ready-to-show", () => {
            /* Despues que ocurre el evento se manda el mensaje*/
            ventanaPrincipal.webContents.send("gastoSeleccionada", seleccionada);
          });
    })

    //EVENTOS DE VER GASTOS
    //Depende de la selección
    ipcMain.on("mostrarLosGastos",(evento,informacion)=>{
        //Crea una nueva ventana para editar el gasto
        cargarHTML("mostrar_gastos");
        ventanaPrincipal.on("ready-to-show", () => {
            /* Despues que ocurre el evento se manda el mensaje*/
            ventanaPrincipal.webContents.send("vistaSeleccionada", informacion);
          });
    })
}

//Ejecuta cargarInicio cuando la app está lista
app.on("ready", cargarInicio);

//Barra de Menu OPCIONAL
const templateMenu = [
    { 
        label:"Salir",
            accelerator: process.platform ==='darwin' ? "command+Q" : "Ctrl+Q",
            click(){
                //Cierra la aplicación
                app.quit();
            }
    },
];

if(!app.isPackaged){
    templateMenu.push({
        label: "DevTools",
        submenu:[
            {
                label: "Mostrar/Ocultar DevTools",
                click(item,focusedWindow){
                    focusedWindow.toggleDevTools();
                },
            },
            {
                role: "Reload"
            }
        ]
    })

}
