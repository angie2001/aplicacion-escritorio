const {ipcRenderer, ipcMain}=require("electron");
const moment = require('moment');
const {
    accesoCuentas,
    buscarCuenta,
    verificarExisteC,
    accesoCategorias,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    accesoGastos,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");

document.addEventListener("DOMContentLoaded",()=>{
    let inicioCuenta={email:""};
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        inicioCuenta= {email: datos.email};
        ipcRenderer.on('gastoSeleccionada',(evento1,seleccionada)=>{
            todo(inicioCuenta, seleccionada);
        })
    })
    
    function todo(inicioCuenta, seleccionada) {
        let gastoEliminar1 = document.getElementById('gastoEliminar1');
        let gastoEliminar2 = document.getElementById('gastoEliminar2');
        let gastoEliminar3 = document.getElementById('gastoEliminar3');
        let gastoEliminar4 = document.getElementById('gastoEliminar4');
        gastoEliminar1.textContent= ("Fecha: "+seleccionada.fecha);
        gastoEliminar2.textContent= ("Descripción: "+seleccionada.descripcion);
        gastoEliminar3.textContent= ("Monto: "+ seleccionada.gasto)
        gastoEliminar4.textContent= ("Categoría: "+seleccionada.categoria);
        console.log(seleccionada);

        
        const botonBorrarGasto = document.getElementById("botonBorrarGasto");
        botonBorrarGasto.addEventListener("click",(evento)=>{
            evento.preventDefault();
            let datos_gastos = accesoGastos();   
            const radioSi = document.getElementById("radioSi");
            
            if(radioSi.checked){
                let capturaGasto ={opcion:
                    "Categoría: " +
                    seleccionada.categoria +
                    "    " +
                    "Gasto: " +
                    seleccionada.gasto +
                    "    " +
                    "Descripción: " +
                    seleccionada.descripcion +
                    "    " +
                    "Fecha: " +
                    seleccionada.fecha};
                datos_gastos = buscarGastoEliminar(datos_gastos, capturaGasto);
                ipcRenderer.send('nombreNuevaCategoria');
            }
            
            

        })
    }

})