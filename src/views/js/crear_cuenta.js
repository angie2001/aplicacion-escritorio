//Librerias necesarias
const {ipcRenderer}=require("electron");
const {
  accesoCuentas,
  buscarCuenta,
  verificarExisteC,
  accesoCategorias,
  buscarCategoria,
  buscarCategoriaEditar,
  cambioNombreCategoriaRegistro,
  accesoGastos,
  gastoParaRegistrar1,
  gastoParaRegistrar2,
  buscarCategoriaEliminar,
  eliminaciondeCategoria1,
  eliminaciondeCategoria2,
  buscarGastoEditar,
  buscarGastoEliminar,
  vistaGeneral,
  vistaCategoria,
  vistaMes,
} = require("./data");


//ÁRBOL DOM 
    document.addEventListener("DOMContentLoaded",()=>{
        const boton_aceptar = document.getElementById("boton_aceptar");
        boton_aceptar.addEventListener("click",(evento)=>{
            evento.preventDefault();
            const textoUsuario = document.getElementById("textoUsuario");
            const entrada1 = textoUsuario.value;
            const textoEmail = document.getElementById("textoEmail");
            const entrada2 = textoEmail.value;
            const textoPassword = document.getElementById("textoPassword");
            const entrada3 = textoPassword.value;

            const nuevaCuenta = {
                usuario: entrada1,
                email: entrada2,
                password: entrada3
            }
            let datos = accesoCuentas();
            let indice = 0;

            datos,indice = buscarCuenta(datos,nuevaCuenta);
            
            if(indice === -1){
              alert(`Nueva cuenta: ${entrada2} `);
              ipcRenderer.send("inicioLaSesion", nuevaCuenta)

            }else{
              alert(`Intente otro email`);
              ipcRenderer.send("nuevaCuenta", nuevaCuenta)
            }
        })
})

