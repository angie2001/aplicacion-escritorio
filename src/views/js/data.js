//librerias necesarias
const fs = require("fs");
const path = require("path");
const bcrypt = require("bcrypt");
const numeroSaltos = 10;

//Genera la ruta del archivo JSON
const rutaArchivo = path.join(__dirname, "datos_cuentas_registro.json");
const rutaArchivo1 = path.join(__dirname, "datos_categorias_registro.json");
const rutaArchivo2 = path.join(__dirname, "datos_gastos_registro.json");

/**
 *
 * @returns Se obtienen los datos registrados en el archivo JSON datos_cuentas_registro
 */
function accesoCuentas () {
  //Verifica si el archivo JSON existe en directorio
  if (fs.existsSync(rutaArchivo)) {
    //Abre el archivo
    let archivo = fs.readFileSync(rutaArchivo);

    //Lee la informacion del archivo
    let datos = JSON.parse(archivo);

    return datos;
  }
};

/**
 *
 * @returns Se obtienen los datos de las categorias registradas en el archivo JSON datos_categorias_registro
 */
function accesoCategorias(){
  //Verifica si el archivo JSON existe en directorio
  if (fs.existsSync(rutaArchivo1)) {
    //Abre el archivo
    let archivo = fs.readFileSync(rutaArchivo1);

    //Lee la informacion del archivo
    let datos_cat = JSON.parse(archivo);

    return datos_cat;
  }
};

/**
 *
 * @returns Se obtienen los gastos registrados en el archivo JSON datos_gastos_registro
 */
function accesoGastos () {
  //Verifica si el archivo JSON existe en directorio
  if (fs.existsSync(rutaArchivo2)) {
    //Abre el archivo
    let archivo = fs.readFileSync(rutaArchivo2);

    //Lee la informacion del archivo
    let datos_gastos = JSON.parse(archivo);

    return datos_gastos;
  }
};

/**
 *
 * @param {object} cuenta es de donde se obtiene el password de la cuenta ingresada
 */
function codificarClave (cuenta) {
  let nuevaClave;
  //Codifica la clave ingresada por el usuario según el numeroSaltos
  //numeroSaltos => el número de rondas utilizadas para cifrar un hash
  nuevaClave = bcrypt.hashSync(cuenta.password, numeroSaltos);

  return nuevaClave;
};

/**
 *
 * @param {object} datos Son las cuentas registradas en datos_cuentas_registro.json
 * @param {object} nuevaCuenta Es la nueva cuenta que se va a incluir en el datos_cuentas_registro.json
 * @returns Actualización de los registros del archivo datos_cuentas_registro.json
 */
function buscarCuenta (datos, nuevaCuenta){
  let contador = 0;
  //Verifica si algún parámetro esta vacio (usuario,email y password)
  if (
    nuevaCuenta.usuario == "" ||
    nuevaCuenta.email == "" ||
    nuevaCuenta.password == ""
  ) {
    console.log("Usted no a insertado ningún caracter");
    return;
  } else {
    contador = 1;
  }

  //Se verifica si existe una cuenta registrada con el mismo email
  const indice = datos.cuentas.findIndex((b) => b.email === nuevaCuenta.email);

  if (indice === -1 && contador == 1) {
    //Se llama a la función codificarClave para codificar la clave
    nuevaCuenta.password = codificarClave(nuevaCuenta);

    //Se agrega una nueva cuenta
    datos.cuentas.push(nuevaCuenta);

    //Se modifica el archivo JSON
    let contenido = JSON.stringify(datos);
    fs.writeFileSync(rutaArchivo, contenido);

    console.log("-------Se creó una nueva cuenta---------");
    console.log("");
    return datos,indice;
  } else {
    console.log("Intente otro email");
    console.log("");
    return datos,indice;
  }
};

/**
 *
 * @param {object} datos Son las cuentas cargadas desde el archivo datos_cuentas_registro.json
 * @param {object} inicioCuenta Es la cuenta que digitó el usuario y con la que se realizará la verificación
 * de existencia en datos
 * @returns {Int}  Para poder acceder al menú de sesión
 */
function verificarExisteC(datos, inicioCuenta){
  //Verifica si algún parámetro está vacío (email, passwaord)
  if (inicioCuenta.email == "" || inicioCuenta.password == "") {
    return;
  } else {
    let existe = false;
    //Recorre el arreglo cuentas para verificar si está registrado el email
    for (const validacion of datos.cuentas) {
      if (validacion.email == inicioCuenta.email) {
        cuenta = inicioCuenta.password;
        existe = bcrypt.compareSync(cuenta, validacion.password);
      }
    } 
    if (existe == true) {
      console.log("SE PUDO INICIAR SESIÓN");
    }else{
      console.log("DATOS INCORRECTOS");
    }
    return existe;
  }
};

/**
 *
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} nueva_cat Es la nueva categoría digitada por el usuario y la que se va a incluir en el archivo datos_categorias_registro.json
 * @returns {object} La actualización del arreglo del archivo datos_categorias_registro.json
 */
function buscarCategoria (inicioCuenta, datos_cat, nueva_cat){
  let contador = 1;

  //Se crea un objeto que contiene el email y el nombre de la nueva categoria
  let CategoriaParaAgregar = {
    email: inicioCuenta.email,
    categoria: nueva_cat.nombre,
  };

  //Se valida si existe una categoría con el mismo nombre asociada al email
  for (const validacion of datos_cat.categorias_cuentas) {
    if (CategoriaParaAgregar.email == validacion.email) {
      if (CategoriaParaAgregar.categoria == validacion.categoria) {
        console.log("Ya se tiene una categoría con este nombre");
        contador = 0;
      }
    }
  }
  if (contador == 1) {
    //Se agrega la nueva categoria
    datos_cat.categorias_cuentas.push(CategoriaParaAgregar);

    //Se modifica el archivo JSON
    let contenido = JSON.stringify(datos_cat);
    fs.writeFileSync(rutaArchivo1, contenido);
    console.log("Se creó una nueva categoria");
    return datos_cat,contador;
  } else {
    console.log("Intente otro nombre de categoría");
    return datos_cat,contador;
  }
};

/**
 *
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} nueva_cat Es el nuevo nombre que va a tener una categoría ya registrada
 * @returns {Int} Para verificar si existe la categoría asociada a la sesión inicida
 */
function buscarCategoriaEditar (inicioCuenta, datos_cat, nueva_cat) {
  let aceptoCambio = 1;
  //Se crea un objeto que contiene el email y el nombre de la categoría digitados por el usuario
  let CategoriaParaAgregar = {
    email: inicioCuenta.email,
    categoria: nueva_cat.nombre,
  };

  //Se valida si existe una categoria asociada con el email
  console.log(nueva_cat.nombre);
  for (const validacion of datos_cat.categorias_cuentas) {
    if (CategoriaParaAgregar.email == validacion.email) {
      if (CategoriaParaAgregar.categoria == validacion.categoria) {
        aceptoCambio = 0;
      }
    }
  }
  return aceptoCambio;
};

/**
 *
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} nueva_cat Nombre de la categoría que se va a eliminar
 * @returns {Int} Para verificar si la categoría a eliminar existe y está asociada con la sesión iniciada
 */
function buscarCategoriaEliminar (inicioCuenta, datos_cat, nueva_cat){
  let aceptoCambio = 1;
  //Se crea un objeto que contiene el email y el nombre de la categoría digitados por el usuario
  let CategoriaParaAgregar = {
    email: inicioCuenta.email,
    categoria: nueva_cat.categoria,
  };
  //Se valida si existe una categoria asociada con el email
  for (const validacion of datos_cat.categorias_cuentas) {
    if (CategoriaParaAgregar.email == validacion.email) {
      if (CategoriaParaAgregar.categoria == validacion.categoria) {
        aceptoCambio = 0;
      }
    }
  }
  return aceptoCambio;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} nuevoNombreCategoria El nuevo nombre de la categoría
 * @returns {object,object} Los datos actualizados de la categoria y en los gastos
 */
function cambioNombreCategoriaRegistro (
  datos_gastos,
  datos_cat,
  inicioCuenta,
  nuevoNombreCategoria
) {
  //Se crean unas variables de control
  let contador = 1;
  let indice = -1;
  let idx = -1;
  //Se verifica en que posición esta la categoria a editar
  for (const validacion of datos_cat.categorias_cuentas) {
    indice = indice + 1;
    if (inicioCuenta.email == validacion.email) {
      if (validacion.categoria == nuevoNombreCategoria.categoria) {
        contador = 0;
        idx = indice;
      }
    }
  }
  if (contador == 0 && idx > -1) {
    //Se cambia el nombre de la cateogoría en el archivo datos_categorias_registro.json
    datos_cat.categorias_cuentas[idx].categoria = nuevoNombreCategoria.nombre;
    //Se cambia el nombre de la categoría que esta asociada con un gasto
    for (const validacion of datos_gastos.gastos_cuentas) {
      if (validacion.email == inicioCuenta.email) {
        if (validacion.categoria == nuevoNombreCategoria.categoria) {
          validacion.categoria = nuevoNombreCategoria.nombre;
        }
      }
    }
    //Se modifica los archivos JSON datos_gastos_registro.json y datos_categorias_registro.json
    let contenido = JSON.stringify(datos_cat);
    let contenido2 = JSON.stringify(datos_gastos);
    fs.writeFileSync(rutaArchivo1, contenido);
    fs.writeFileSync(rutaArchivo2, contenido2);
    console.log("Nuevo nombre aceptado");
    console.log("Sus categorías registradas son:");
    //Se imprimen las categorías del usuario (actualizadas)
    for (const validacion of datos_cat.categorias_cuentas) {
      if (validacion.email == inicioCuenta.email) {
        console.log(validacion.categoria);
      }
    }
    console.log("*******************************************");
    return datos_gastos, datos_cat;
  } else {
    console.log("No existe una categoría igual");
    return;
  }
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} categoriaConGasto Contiene el parámetro categoría digitada por
 * el usuario para poder registrar un gasto asociado a una categoría
 * @param {object} RegistroGasto Contiene los parámetros de el gasto,descripción y fecha digitados por
 * el usuario para poder registrar un gasto asociado a una categoría
 * @returns {object} Los gastos actualizados del archivo datos_gastos_registro.json
 */
function gastoParaRegistrar1 (
  datos_gastos,
  inicioCuenta,
  categoriaConGasto,
  RegistroGasto
) {
  //Se crea un objeto que contenga email,categoría,gasto,descripción y fecha del nuevo gasto
  let paraAgregarGasto = {
    email: inicioCuenta.email,
    categoria: categoriaConGasto.nombre,
    gasto: RegistroGasto.gasto,
    descripcion: RegistroGasto.descripcion,
    fecha: RegistroGasto.fecha,
  };

  //Se agrega el nuevo gasto al archivo datos_gastos_registro.json
  datos_gastos.gastos_cuentas.push(paraAgregarGasto);

  //Se actualiza el archivio JSON
  let contenido = JSON.stringify(datos_gastos);
  fs.writeFileSync(rutaArchivo2, contenido);

  console.log("Se agregó el gasto");
  console.log(
    "Categoría: " +
      paraAgregarGasto.categoria +
      "    " +
      "Gasto: " +
      paraAgregarGasto.gasto +
      "    " +
      "Descripción: " +
      paraAgregarGasto.descripcion +
      "    " +
      "Fecha: " +
      paraAgregarGasto.fecha
  );
  return datos_gastos;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} RegistroGasto Contiene los parámetros de el gasto,descripción y fecha digitados por
 * el usuario para poder registrar un gasto asociado a una categoría
 * @returns {object} Los gastos actualizados del archivo datos_gastos_registro.json
 */
function gastoParaRegistrar2 (
  datos_gastos,
  inicioCuenta,
  RegistroGasto
) {
  //Se crea un objeto que contiene email,gasto,descripción y la fecha
  //este tiene por defecto la catetegoria = general, porque el gasto no tienen una categoria asociada
  let paraAgregarGasto = {
    email: inicioCuenta.email,
    categoria: "general",
    gasto: RegistroGasto.gasto,
    descripcion: RegistroGasto.descripcion,
    fecha: RegistroGasto.fecha,
  };

  //Se agrega el nuevo gasto en el archivo datos_gastos_registro.json
  datos_gastos.gastos_cuentas.push(paraAgregarGasto);

  //Se actualiza el archivo archivo datos_gastos_registro.json
  let contenido = JSON.stringify(datos_gastos);
  fs.writeFileSync(rutaArchivo2, contenido);
  console.log("Se agregó el gasto");
  console.log(
    "Categoría: " +
      paraAgregarGasto.categoria +
      "    " +
      "Gasto: " +
      paraAgregarGasto.gasto +
      "    " +
      "Descripción: " +
      paraAgregarGasto.descripcion +
      "    " +
      "Fecha: " +
      paraAgregarGasto.fecha
  );
  console.log(datos_gastos);
  return datos_gastos;
};

/**
 * Función para eliminar categoría CON gastos
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} nombredeCategoriaEliminar Nombre de la categoría que se quiere eliminar
 * @returns {object,object} Los datos actualizados de la categoria y en los gastos
 */
function eliminaciondeCategoria1 (
  datos_cat,
  datos_gastos,
  inicioCuenta,
  nombredeCategoriaEliminar
) {
  //Se crea una variable de control
  let contador = -1;
  //Se crea un objeto que contiene el email y la categoría
  let categoriaParaEliminar = {
    email: inicioCuenta.email,
    categoria: nombredeCategoriaEliminar.categoria,
  };

  //Devuelve un objeto que contiene el arreglo sin los gastos asociados a la categoria que se
  //desea eliminar
  let filtro_gastos = datos_gastos.gastos_cuentas.filter(
    (x) =>
      x.email != categoriaParaEliminar.email ||
      x.categoria != categoriaParaEliminar.categoria
  );
  datos_gastos.gastos_cuentas = filtro_gastos;

  //Se elimina la categoría digitada por el usuario del archivo JSON datos_categorias_registro
  for (let indice = 0; indice < datos_cat.categorias_cuentas.length; indice++) {
    if (
      categoriaParaEliminar.email == datos_cat.categorias_cuentas[indice].email
    ) {
      if (
        categoriaParaEliminar.categoria ==
        datos_cat.categorias_cuentas[indice].categoria
      ) {
        datos_cat.categorias_cuentas.splice(indice, 1);
      }
    }
  }

  //Se actualizan los archivos datos_gastos_registro.json y datos_categorias_registro.json
  let contenido = JSON.stringify(datos_gastos);
  let contenido2 = JSON.stringify(datos_cat);
  fs.writeFileSync(rutaArchivo2, contenido);
  fs.writeFileSync(rutaArchivo1, contenido2);
  console.log("Se ha eliminado la categoría y sus gastos con éxito");
  return datos_gastos, datos_cat;
};

/**
 * Función para eliminar categoría SIN gastos
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} nombredeCategoriaEliminar Nombre de la categoría que se quiere eliminar
 * @returns {object,object} Los datos actualizados de la categoria y en los gastos
 */
function eliminaciondeCategoria2 (
  datos_cat,
  datos_gastos,
  inicioCuenta,
  nombredeCategoriaEliminar
) {
  //Se crea un objeto que tiene contenido email y categoría
  let categoriaParaEliminar = {
    email: inicioCuenta.email,
    categoria: nombredeCategoriaEliminar.categoria,
  };
  //Se reasignan los gastos de esta categoría a "general"
  for (const validacion of datos_gastos.gastos_cuentas) {
    if (categoriaParaEliminar.email == validacion.email) {
      if (categoriaParaEliminar.categoria == validacion.categoria) {
        validacion.categoria = "general";
      }
    }
  }

  //Se elimina la categoría
  for (let indice = 0; indice < datos_cat.categorias_cuentas.length; indice++) {
    if (
      categoriaParaEliminar.email == datos_cat.categorias_cuentas[indice].email
    ) {
      if (
        categoriaParaEliminar.categoria ==
        datos_cat.categorias_cuentas[indice].categoria
      ) {
        datos_cat.categorias_cuentas.splice(indice, 1);
      }
    }
  }

  //Se actualizan los archivos datos_gastos_registro.json y datos_categorias_registro.json
  let contenido = JSON.stringify(datos_gastos);
  let contenido2 = JSON.stringify(datos_cat);
  fs.writeFileSync(rutaArchivo2, contenido);
  fs.writeFileSync(rutaArchivo1, contenido2);
  console.log(
    "Se ha eliminado la categoría y sus gastos han pasado a la zona general"
  );
  return datos_gastos, datos_cat;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {Object} capturaGasto Gasto seleccionado por el usuario para editar
 * @param {Object} paraEditarGasto Nombre del parámetro que se va a editar del gasto (categoría, gasto, descripción o fecha)
 * @param {Object} nuevarespuesta Contiene el nuevo valor del parámetro que se editó del gasto
 * @returns {Object} Actulización del archivo datos_gastos_registro.JSON
 */
function buscarGastoEditar (
  datos_gastos
) {
  
  //Se actualizan los datos del archivo datos_gasto_registro.JSON
  let contenido = JSON.stringify(datos_gastos);
  fs.writeFileSync(rutaArchivo2, contenido);
  console.log("Los cambios se realizaron");
  return datos_gastos;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {Object} capturaGasto Gasto seleccionado por el usuario para eliminar
 * @returns {Object} Actulización del archivo datos_gastos_registro.JSON
 */
function buscarGastoEliminar (datos_gastos, capturaGasto) {
  let aceptoCambio = 1;
  let indice = 0;
  //Se busca el gasto seleccionado por el usuario y se elimina del objeto datos_gastos
  //en el archivo datos_gastos_registro.JSON
  for (const validacion of datos_gastos.gastos_cuentas) {
    let paraComprobar =
      "Categoría: " +
      validacion.categoria +
      "    " +
      "Gasto: " +
      validacion.gasto +
      "    " +
      "Descripción: " +
      validacion.descripcion +
      "    " +
      "Fecha: " +
      validacion.fecha;
    if (capturaGasto.opcion == paraComprobar) {
      console.log("El indice de validacion es: " + indice);
      datos_gastos.gastos_cuentas.splice(indice, 1);
    }
    indice = indice + 1;
  }
  //Se actualizan los datos del archivo datos_gasto_registro.JSON
  let contenido = JSON.stringify(datos_gastos);
  fs.writeFileSync(rutaArchivo2, contenido);
  console.log("Los cambios se realizaron");
  return datos_gastos;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 */
function vistaGeneral (datos_gastos, inicioCuenta) {
  //Se utiliza la funcion sort.() para organizar los datos de manera coronológica
  datos_gastos.gastos_cuentas.sort(function (a, b) {
    //Se resta a y b, si el resultado es positivo a se mantiene en su lugar
    //Si el resultado es negativo, b cambia de posicón con a
    //Si es 0 se mantienen ambas posiciones
    //Esta línea ordena los gastos de el más antiguo al más reciente
    return new Date(a.fecha).getTime() - new Date(b.fecha).getTime();
  });

  //Se crea un arreglo donde se mostrará el contenido del arreglo datos_gastos.gastos_cuentas inverso
  //Para que el orden cronológico sea del gasto más reciente al más antiguo
  let alreves = [];
  //Se invierte el arreglo
  for (let i = datos_gastos.gastos_cuentas.length - 1; i >= 0; i--) {
    alreves.push(datos_gastos.gastos_cuentas[i]);
  }

  //Se imprime el arreglo inverso con los gastos asociados a la cuenta
  
  return alreves;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {Object} respuestaFiltro Es el nombre de la categoría con que se quiere realizar el filtro de los gastos
 */
function vistaCategoria (datos_gastos, inicioCuenta, respuestaFiltro) {
  //Se utiliza la funcion sort.() para organizar los datos de manera coronológica
  datos_gastos.gastos_cuentas.sort(function (a, b) {
    //Se resta a y b, si el resultado es positivo a se mantiene en su lugar
    //Si el resultado es negativo, b cambia de posicón con a
    //Si es 0 se mantienen ambas posiciones
    //Esta línea ordena los gastos de el más antiguo al más reciente
    return new Date(a.fecha).getTime() - new Date(b.fecha).getTime();
  });
  //Se crea un arreglo donde se mostrará el contenido del arreglo datos_gastos.gastos_cuentas inverso
  //Para que el orden cronológico sea del gasto más reciente al más antiguo
  let alreves = [];
  //Se invierte el arreglo
  for (let i = datos_gastos.gastos_cuentas.length - 1; i >= 0; i--) {
    alreves.push(datos_gastos.gastos_cuentas[i]);
  }
  //Se imprime el arreglo inverso con los gastos asociados a la cuenta
  console.log("El flitro "+respuestaFiltro.nombre);
  let arrverC = [];
  for (const validacion of alreves) {
    if (
      validacion.email == inicioCuenta.email &&
      validacion.categoria == respuestaFiltro.nombre
    ) {
      console.log(
        "Categoría: " +
          validacion.categoria +
          "    " +
          "Gasto: " +
          validacion.gasto +
          "    " +
          "Descripción: " +
          validacion.descripcion +
          "    " +
          "Fecha: " +
          validacion.fecha
      );
      arrverC.push({
        email:validacion.email,
        categoria:validacion.categoria,
        gasto:validacion.gasto,
        descripcion:validacion.descripcion,
        fecha:validacion.fecha

      })
    }
  }
  return arrverC;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {Object} respuestaFiltro Es el nombre del mes con que se quiere realizar el filtro de los gastos
 */

function vistaMes (datos_gastos, inicioCuenta, respuestaFiltro) {
  //Se crea un arreglo vacío donde estarán guardados los gasto asociados a la cuenta y que son iguales
  //al mes digitado por el usuario
  let arregloFechas = [];
  let comparacion;
  //Se realiza la compración
  //Comparación es una variable de tipo Date, la cual será utilizada para obtener el mes con el que se llenará arregloFechas[]
  if (respuestaFiltro.nombre == "enero") {
    comparacion = new Date("2000/01/01");
  } else if (respuestaFiltro.nombre == "febrero") {
    comparacion = new Date("2000/02/01");
  } else if (respuestaFiltro.nombre == "marzo") {
    comparacion = new Date("2000/03/01");
  } else if (respuestaFiltro.nombre == "abril") {
    comparacion = new Date("2000/04/01");
  } else if (respuestaFiltro.nombre == "mayo") {
    comparacion = new Date("2000/05/01");
  } else if (respuestaFiltro.nombre == "junio") {
    comparacion = new Date("2000/06/01");
  } else if (respuestaFiltro.nombre == "julio") {
    comparacion = new Date("2000/07/01");
  } else if (respuestaFiltro.nombre == "agosto") {
    comparacion = new Date("2000/08/01");
  } else if (respuestaFiltro.nombre == "septiembre") {
    comparacion = new Date("2000/09/01");
  } else if (respuestaFiltro.nombre == "octubre") {
    comparacion = new Date("2000/10/01");
  } else if (respuestaFiltro.nombre == "noviembre") {
    comparacion = new Date("2000/11/01");
  } else if (respuestaFiltro.nombre == "diciembre") {
    comparacion = new Date("2000/12/01");
  } else {
    console.log("No existe ese mes");
    return;
  }
  //Variable de control
  let elmes;
  //Llenar arregloFechas[]
  for (const validacion of datos_gastos.gastos_cuentas) {
    elmes = new Date(validacion.fecha);
    if (validacion.email == inicioCuenta.email) {
      if (elmes.getMonth() + 1 == comparacion.getMonth() + 1) {
        arregloFechas.push(validacion);
      }
    }
  }
  //Se utiliza la funcion sort.() para organizar los datos de manera coronológica
  arregloFechas.sort(function (a, b) {
    //Se resta a y b, si el resultado es positivo a se mantiene en su lugar
    //Si el resultado es negativo, b cambia de posicón con a
    //Si es 0 se mantienen ambas posiciones
    //Esta línea ordena los gastos de el más antiguo al más reciente
    return new Date(a.fecha).getTime() - new Date(b.fecha).getTime();
  });
  //Se crea un arreglo donde se mostrará el contenido del arreglo datos_gastos.gastos_cuentas inverso
  //Para que el orden cronológico sea del gasto más reciente al más antiguo
  let alreves = [];
  //Se invierte el arreglo
  for (let i = arregloFechas.length - 1; i >= 0; i--) {
    alreves.push(arregloFechas[i]);
  }
  //Se imprimen los gastos sociados a la cuenta
  let arrverM = [];
  for (validacion of alreves) {
    console.log(
      "Categoría: " +
        validacion.categoria +
        "    " +
        "Gasto: " +
        validacion.gasto +
        "    " +
        "Descripción: " +
        validacion.descripcion +
        "    " +
        "Fecha: " +
        validacion.fecha
    );
    arrverM.push({
      email:validacion.email,
      categoria:validacion.categoria,
      gasto:validacion.gasto,
      descripcion:validacion.descripcion,
      fecha:validacion.fecha

    })
  }
  return arrverM;
};

// function capturaSesion(cuenta){
//   let inicioCuenta = cuenta.email;

//   return inicioCuenta
// }
/**
 * Funciones que son exportadas al archivo app.js
 */
module.exports = {
  accesoCuentas,
  buscarCuenta,
  verificarExisteC,
  accesoCategorias,
  buscarCategoria,
  buscarCategoriaEditar,
  cambioNombreCategoriaRegistro,
  accesoGastos,
  gastoParaRegistrar1,
  gastoParaRegistrar2,
  buscarCategoriaEliminar,
  eliminaciondeCategoria1,
  eliminaciondeCategoria2,
  buscarGastoEditar,
  buscarGastoEliminar,
  vistaGeneral,
  vistaCategoria,
  vistaMes,
};
