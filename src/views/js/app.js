/**
 * Funciones y variable que provienen de opciones.js
 */
 const moment = require('moment');
 const {
   menu,
   crearNuevaCuenta,
   datosAcceso,
   INICIO_S,
   CREAR_C,
   SALIR,
   menu_s,
   VER_G,
   REGISTRAR_G,
   BUSCAR_G,
   VER_C,
   CATEGORIAS,
   CREAR_CAT,
   CERRAR_S,
   nuevaCategoria,
   cambioNombreCategoria,
   menu_cat,
   EDITAR_CAT,
   ELIMINAR_CAT,
   VOLVER,
   crearGasto,
   preguntaGasto,
   categoria_gasto,
   nombreparaEliminarCat,
   preguntaConfirmacion,
   menuDinamicoGastos,
   menuCadaGasto,
   EDITAR_GASTO,
   ELIMINAR_GASTO,
   SALIR_GASTO,
   parametrodelGasto,
   nuevoValorParametroGasto,
   parametroparafiltrar,
   nombredelfiltro,
 } = require("./opciones");
 
 /**
  * Funciones que provienen de data.js
  */
 const {
   accesoCuentas,
   buscarCuenta,
   verificarExisteC,
   accesoCategorias,
   buscarCategoria,
   buscarCategoriaEditar,
   cambioNombreCategoriaRegistro,
   accesoGastos,
   gastoParaRegistrar1,
   gastoParaRegistrar2,
   buscarCategoriaEliminar,
   eliminaciondeCategoria1,
   eliminaciondeCategoria2,
   buscarGastoEditar,
   buscarGastoEliminar,
   vistaGeneral,
   vistaCategoria,
   vistaMes,
 } = require("./data");
 
 /**
  *
  * @param {object} inicioCuenta Es la cuenta que digitó el usuario y con la que se realizará la verificación
  */
 async function menusesion(inicioCuenta) {
   //Se cargan los datos de los archivos JSON datos_gastos_registro y datos_categorias_registro
   let datos_categorias = await accesoCategorias();
   let datos_gastos = await accesoGastos();
 
   let opcion_i = 0;
   //Ciclo para mantener la sesión iniciada
   while (opcion_i != CERRAR_S) {
     let dato_i = (opcion_i = await menu_s());
     opcion_i = dato_i.opcion2;
     //Opciones del menu después de inicio de sesión
     switch (opcion_i) {
       case VER_G:
         //Para visualizar el historico de todos los gastos
         console.log("Quiere ver el Historico");
         //Pregunta si se desea ver todo o por filtro
         let esperaconfirmacion = await preguntaConfirmacion();
         esperaconfirmacion.pregunta = esperaconfirmacion.pregunta.toLowerCase();
         if (esperaconfirmacion.pregunta == "si") {
           console.log("VISTA HISTORICO GENERAL");
           console.log("--------------------------------------------------");
           //Vista general de los gastos
           await vistaGeneral(datos_gastos, inicioCuenta);
           console.log("--------------------------------------------------");
         } else {
           console.log("Vista filtro");
           console.log("Los filtros a utilizar son: categoria o el mes");
           //Vista con filtro
           let vistaFiltro = await parametroparafiltrar();
           vistaFiltro.filtro = vistaFiltro.filtro.toLowerCase();
           if (vistaFiltro.filtro == "categoria") {
             //Filtro Categoría
             let respuestaFiltro = await nombredelfiltro(vistaFiltro);
             let existeCategoria = await buscarCategoriaEditar(
               inicioCuenta,
               datos_categorias,
               respuestaFiltro
             );
             //Se confirma que existe la categoría
             if (existeCategoria == 0 || respuestaFiltro.nombre == "general") {
               console.log(
                 "La " +
                   vistaFiltro.filtro +
                   " filtrada es: " +
                   respuestaFiltro.nombre
               );
               console.log("--------------------------------------------------");
               await vistaCategoria(datos_gastos, inicioCuenta, respuestaFiltro);
               console.log("--------------------------------------------------");
             } else {
               console.log("La categoria no esta registrada en esta cuenta");
             }
           } else {
             if (vistaFiltro.filtro == "mes") {
               //Filtro por mes
               let respuestaFiltro = await nombredelfiltro(vistaFiltro);
               respuestaFiltro.nombre = respuestaFiltro.nombre.toLowerCase();
               console.log(
                 "El " +
                   vistaFiltro.filtro +
                   " filtrado es: " +
                   respuestaFiltro.nombre
               );
 
               await vistaMes(datos_gastos, inicioCuenta, respuestaFiltro);
             } else {
               console.log("Filtro no valido");
             }
           }
         }
         break;
 
       //Para registrar un gasto
       case REGISTRAR_G:
         console.log("Registrar gasto");
         //Se preguntan los parámetros para crear el gasto
         let RegistroGasto = await crearGasto();
         //Se verifica que la fecha sea una fecha
         let lafecha = Date.parse(RegistroGasto.fecha);
         console.log(lafecha);
         let fechacambio = moment(lafecha).format('YYYY/DD/MM');
         console.log(fechacambio);
         let f1 = Date.parse(fechacambio);
         if (isNaN(f1)) {
           console.log("La fecha ingresada no es valida");
         } else {
           //Se verifica el valor del gasto (que sea valor positivo y mayor a 0)
           RegistroGasto.gasto = parseInt(RegistroGasto.gasto);
           if (RegistroGasto.gasto > 0) {
             let respuesta = await preguntaGasto();
             //Si quiere asociar un gasto a una categoría
             respuesta.pregunta = respuesta.pregunta.toLowerCase();
             if (respuesta.pregunta == "si") {
               console.log("respondio si");
               let categoriaConGasto = await categoria_gasto();
               let existeCategoria = await buscarCategoriaEditar(
                 inicioCuenta,
                 datos_categorias,
                 categoriaConGasto
               );
               //Se verifica que existe la categoria en esa cuenta
               if (existeCategoria == 0) {
                 //Se registra el gasto con categoría
                 //ditada
                 RegistroGasto.fecha = fechacambio;
                 datos_gastos = await gastoParaRegistrar1(
                   datos_gastos,
                   inicioCuenta,
                   categoriaConGasto,
                   RegistroGasto
                 );
               } else {
                 console.log("No existe esa categoria en la cuenta");
                 console.log("Intente nuevamente ingresar el gasto");
               }
             } else {
               console.log("respondio no");
               //Se registra el gasto en general
               RegistroGasto.fecha = fechacambio;
               datos_gastos = await gastoParaRegistrar2(
                 datos_gastos,
                 inicioCuenta,
                 RegistroGasto
               );
             }
           } else {
             console.log("Valor de gasto no permitido");
           }
         }
 
         break;
 
       //Pra buscar un gasto en específico
       case BUSCAR_G:
         console.log("Buscar gasto");
         //Se llama al menú dinámico con los gastos de la cuenta
         let capturaGasto = await menuDinamicoGastos(datos_gastos, inicioCuenta);
         //Se llama el menuGasto para (editar o eliminar)
         await menuGasto(
           datos_gastos,
           datos_categorias,
           inicioCuenta,
           capturaGasto
         );
 
         break;
 
       //Para ver categorías
       case VER_C:
         console.log("Las categorias registradas son:");
         for (const validacion of datos_categorias.categorias_cuentas) {
           if (validacion.email == inicioCuenta.email) {
             console.log(validacion.categoria);
           }
         }
       //para mostrar categorías registradas
       case CATEGORIAS:
         //Menu para (editar o eliminar) categoría
         await menuCategoria(datos_categorias, inicioCuenta, datos_gastos);
         break;
       //Para crear categoría
       case CREAR_CAT:
         console.log("Crear Categoría");
         //Se ingresa el nombre de la nueva categoría
         let nueva_cat = await nuevaCategoria();
         //Se llama a la función para agregar la categoría
         datos_categorias = await buscarCategoria(
           inicioCuenta,
           datos_categorias,
           nueva_cat
         );
         break;
 
       //Para cerrar la sesión
       case CERRAR_S:
         console.log("Cerrar sesión");
         console.log("----------------------------");
         break;
     }
   }
 }
 
 /**
  *
  * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
  * @param {object} datos_categoria Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
  * @param {object} inicioCuenta Es la cuenta que digitó el usuario y con la que se realizará la verificación
  * @param {Object} capturaGasto Gasto seleccionado por el usuario para dar opciones del gasto
  */
 async function menuGasto(
   datos_gastos,
   datos_categorias,
   inicioCuenta,
   capturaGasto
 ) {
   let opcion_i = 1;
   //Ciclo para mantener el menú del gasto
   while (opcion_i == EDITAR_GASTO) {
     //Se utiliza la funcion para llamar al menú de cada gasto
     let dato_i = (opcion_i = await menuCadaGasto());
     opcion_i = dato_i.opcion;
 
     switch (opcion_i) {
       //Para editar un gasto
       case EDITAR_GASTO:
         //Saber que parámetro se va a editar
         let paraEditarGasto = await parametrodelGasto();
         //Nuevo valor del parámetro
         let nuevarespuesta = await nuevoValorParametroGasto(paraEditarGasto);
         console.log("El parámetro es: " + paraEditarGasto.pregunta);
         console.log("Nuevo asignación: " + nuevarespuesta.nombre);
         paraEditarGasto.pregunta = paraEditarGasto.pregunta.toLowerCase();
         //Si se edita la categoria...
         if (paraEditarGasto.pregunta == "categoria") {
           //Si existe una categoría anterior
           let existeCategoria = await buscarCategoriaEditar(
             inicioCuenta,
             datos_categorias,
             nuevarespuesta
           );
           //Si existe la categoría o está en general
           if (existeCategoria == 0 || nuevarespuesta.nombre == "general") {
             console.log("existe esa categoria");
             //SE edita el gasto
             datos_gastos = await buscarGastoEditar(
               datos_gastos,
               capturaGasto,
               paraEditarGasto,
               nuevarespuesta
             );
           } else {
             console.log("Esa categoría no esta registrada en la cuenta");
           }
         } else {
           //Si se edita el valor del gasto...
           if (paraEditarGasto.pregunta == "gasto") {
             nuevarespuesta.nombre = parseInt(nuevarespuesta.nombre);
             //Se verifica que el gasto no sea de un monto negativo o 0
             if (nuevarespuesta.nombre > 0) {
               //Se edita el gasto
               datos_gastos = await buscarGastoEditar(
                 datos_gastos,
                 capturaGasto,
                 paraEditarGasto,
                 nuevarespuesta
               );
             } else {
               console.log("Valor no permitido");
             }
           } else {
             //Si se edita la descripción...
             if (paraEditarGasto.pregunta == "descripcion") {
               //Se edita el gasto
               datos_gastos = await buscarGastoEditar(
                 datos_gastos,
                 capturaGasto,
                 paraEditarGasto,
                 nuevarespuesta
               );
             } else {
               //Si se edita la fecha...
               if (paraEditarGasto.pregunta == "fecha") {
                 console.log("esta ingresando a la columna fecha");
                 //Se verifica que sea una fecha válida
                 let lafecha = Date.parse(nuevarespuesta.nombre);
                 if (isNaN(lafecha)) {
                   console.log("Fecha no valida");
                 } else {
                   //Se edita el gasto
                   datos_gastos = await buscarGastoEditar(
                     datos_gastos,
                     capturaGasto,
                     paraEditarGasto,
                     nuevarespuesta
                   );
                 }
               } else {
                 console.log("No existe ese parámetro dentro de la aplicación");
               }
             }
           }
         }
         await menusesion(inicioCuenta);
 
         break;
 
       //Para eliminar un gasto
       case ELIMINAR_GASTO:
         console.log(
           "Usted escogió eliminar el gasto: \n" + capturaGasto.opcion
         );
         //Se confirma si se desea eliminar o no el gasto
         let confirmacionUsuario = await preguntaConfirmacion();
         confirmacionUsuario.pregunta =
           confirmacionUsuario.pregunta.toLowerCase();
         //Si se elimina el gasto
         if (confirmacionUsuario.pregunta == "si") {
           datos_gastos = await buscarGastoEliminar(datos_gastos, capturaGasto);
           console.log("Se ha eliminado el gasto");
         }
         //No se elimina el gasto
         else {
           console.log("Se cancela la acción");
         }
 
         break;
 
       //Para salir del gasto
       case SALIR_GASTO:
         //Se devuelve al menú de sesión
         console.log("Saliendo del gasto ...");
 
         break;
     }
   }
 }
 
 /**
  *
  * @param {object} datos_categoria Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
  * @param {object} inicioCuenta Es la cuenta que digitó el usuario y con la que se realizará la verificación
  * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
  */
 async function menuCategoria(datos_categorias, inicioCuenta, datos_gastos) {
   let opcion_i = 0;
   //Ciclo para mantener el menú de la categoría
   while (opcion_i != VOLVER) {
     //Se utiliza la función para llamar el menú para la categoría
     let dato_i = (opcion_i = await menu_cat());
     opcion_i = dato_i.opcion2;
 
     switch (opcion_i) {
       //PAra editar la cateogría
       case EDITAR_CAT:
         //Se muestran las categorías registradas a la cuenta
         console.log("Las categorias registradas son:");
         for (const validacion of datos_categorias.categorias_cuentas) {
           if (validacion.email == inicioCuenta.email) {
             console.log(validacion.categoria);
           }
         }
         console.log();
         //Se ingresa el nuevo nombre de la categoría
         let nuevoNombreCategoria = await cambioNombreCategoria();
         //Se busca si existe una categoría ya registrada con el mismo nombre
         let aceptoCambio = await buscarCategoriaEditar(
           inicioCuenta,
           datos_categorias,
           nuevoNombreCategoria.nombre
         );
         if (aceptoCambio == 1) {
           //Se edita el nombre de la categoría
           datos_gastos,
             (datos_categorias = await cambioNombreCategoriaRegistro(
               datos_gastos,
               datos_categorias,
               inicioCuenta,
               nuevoNombreCategoria
             ));
         } else {
           console.log("Ya se tiene una categoría con el nuevo nombre");
           console.log("Intente ingresar otro nombre");
         }
 
         break;
 
       //Para eliminar una categoría
       case ELIMINAR_CAT:
         //Se muestran las categorías registradas a la cuenta
         console.log("Las categorias registradas son:");
         for (const validacion of datos_categorias.categorias_cuentas) {
           if (validacion.email == inicioCuenta.email) {
             console.log(validacion.categoria);
           }
         }
         //Se ingresa el nombre de la cateogoría que se quiere eliminar
         let nombredeCategoriaEliminar = await nombreparaEliminarCat();
         //Se verifica que la categoría exista en la cuenta
         let existeCategoria = await buscarCategoriaEliminar(
           inicioCuenta,
           datos_categorias,
           nombredeCategoriaEliminar
         );
         if (existeCategoria == 0) {
           console.log("SI EXISTE LA CATEGORIA....");
           console.log("Quiere eliminar los gastos asociados con la categoría");
           //Se preunta al usuario si quiere eliminar los gastos asociados a la categoría a eliminar
           let confirmacionUsuario = await preguntaConfirmacion();
           confirmacionUsuario.pregunta =
             confirmacionUsuario.pregunta.toLowerCase();
 
           if (confirmacionUsuario.pregunta == "si") {
             console.log("Dijo si");
             //Se eliminan la categoría y los gastos asosiados a esa categoría
             datos_gastos,
               (datos_categorias = await eliminaciondeCategoria1(
                 datos_categorias,
                 datos_gastos,
                 inicioCuenta,
                 nombredeCategoriaEliminar
               ));
           } else {
             //Se elimina la categoría pero SIN gastos (los gastos pasan a "general")
             datos_gastos,
               (datos_categorias = await eliminaciondeCategoria2(
                 datos_categorias,
                 datos_gastos,
                 inicioCuenta,
                 nombredeCategoriaEliminar
               ));
           }
         } else {
           console.log("No existe esa categoría");
           console.log("Intente nuevamente");
         }
 
         break;
 
       //Para salir del menú de categoría
       case VOLVER:
         console.log("Volver");
         //Se devuelve al menú de sesión
         console.log("----------------------------");
         break;
     }
   }
 }
 
 /**
  * Menú principal de la aplicación
  */
 async function main() {
   //Se cargan los datos de las cuentas registradas del archivo JSON datos_cuentas_registradas.json
   let datos = await accesoCuentas();
   let opcion = 0;
 
   //Ciclo para mantener el menú principal de la aplicación
   while (opcion != SALIR) {
     //Se utiliza la función para llamar al menú principal
     let dato = (opcion = await menu());
     opcion = dato.opcion;
 
     switch (opcion) {
       //Para iniciar una sesión
       case INICIO_S:
         console.log("SE VA A INICIAR SESION");
         //Se ingresa el email y password de la sesión que se quiere iniciar
         let inicioCuenta = await datosAcceso();
         //Verifica si la cuenta ya esta registrada
         let verificacion_c = await verificarExisteC(datos, inicioCuenta);
         if (verificacion_c == 1) {
           //Se inicia la sesión y se llama el menu de la sesión
           await menusesion(inicioCuenta);
         }
         console.log("----------------------------");
         break;
 
       //Para crear una cuenta
       case CREAR_C:
         console.log("SE VA A CREAR UNA CUENTA");
         //Se ingresan los parámetros (usuario, email, password) para crear una nueva cuenta
         let nuevaCuenta = await crearNuevaCuenta();
         //Busca si existe una cuenta con un mismo email y si no es así crea una nueva cuenta
         datos = await buscarCuenta(datos, nuevaCuenta);
 
         break;
 
       //Para salir de la aplicación
       case SALIR:
         //Se sale de la aplicación
         console.log("SE VA A SALIR DE LA APLICACIÓN");
         console.log("... Muchas gracias por usar esta app :) ...");
         break;
     }
   }
 }
 
 main();
 