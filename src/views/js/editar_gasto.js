const {ipcRenderer, ipcMain}=require("electron");
const moment = require('moment');
const {
    accesoCuentas,
    buscarCuenta,
    verificarExisteC,
    accesoCategorias,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    accesoGastos,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");

document.addEventListener("DOMContentLoaded",()=>{
    let inicioCuenta={email:""};
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        inicioCuenta= {email: datos.email};
        ipcRenderer.on('gastoSeleccionada',(evento1,seleccionada)=>{
            todo(inicioCuenta, seleccionada);
        })
    })
    
    function todo(inicioCuenta, seleccionada) {
        let gastoEditar1 = document.getElementById('gastoEditar1');
        let gastoEditar2 = document.getElementById('gastoEditar2');
        let gastoEditar3 = document.getElementById('gastoEditar3');
        let gastoEditar4 = document.getElementById('gastoEditar4');
        gastoEditar1.textContent= ("Fecha: "+seleccionada.fecha);
        gastoEditar2.textContent= ("Descripción: "+seleccionada.descripcion);
        gastoEditar3.textContent= ("Monto: "+ seleccionada.gasto)
        gastoEditar4.textContent= ("Categoría: "+seleccionada.categoria);
        console.log(seleccionada);

        
        const botonEditarGasto = document.getElementById("botonEditarGasto");
        botonEditarGasto.addEventListener("click",(evento)=>{
            evento.preventDefault();
            let datos_gastos = accesoGastos();
            let datos_categorias = accesoCategorias();
            const textoFecha = document.getElementById("textoFecha");
            const entrada1 = textoFecha.value;
            const textoDescripcion = document.getElementById("textoDescripcion");
            const entrada2 = textoDescripcion.value;
            const textoMonto = document.getElementById("textoMonto");
            const entrada3 = textoMonto.value;
            const textoCategoria = document.getElementById("textoCategoria");
            const entrada4 = textoCategoria.value;
            const opcionSi = document.getElementById("opcionSi");
            const opcionNo = document.getElementById("opcionNo");

            let gastoEditado ={
                email:inicioCuenta.email,
                categoria:entrada4,
                gasto:entrada3,
                descripcion:entrada2,
                fecha:entrada1,
            }
            let fechacambio = moment(entrada1).format('YYYY/MM/DD');
            gastoEditado.fecha = fechacambio;

            if(opcionSi.checked){
                let nueva_cat = {nombre: entrada4};
                let aceptoCambio = buscarCategoriaEditar(
                    inicioCuenta,
                    datos_categorias,
                    nueva_cat
                    );
                if(aceptoCambio == 0 || nueva_cat.nombre =="general"){
                    console.log("existe categoría");
                    for(const validacion of datos_gastos.gastos_cuentas){
                        if(validacion.email == seleccionada.email
                            && validacion.fecha == seleccionada.fecha
                            && validacion.descripcion == seleccionada.descripcion
                            && validacion.gasto == seleccionada.gasto
                            && validacion.categoria == seleccionada.categoria){
                                
                                if(gastoEditado.email ==""){
                                    validacion.email=seleccionada.email;
                                }
                                else{
                                    validacion.email = gastoEditado.email;
                                }

                                if(gastoEditado.fecha ==""){
                                    validacion.fecha=seleccionada.fecha;
                                }
                                else{
                                    validacion.fecha = gastoEditado.fecha;
                                }

                                if(gastoEditado.descripcion ==""){
                                    validacion.descripcion=seleccionada.descripcion;
                                }
                                else{
                                    validacion.descripcion = gastoEditado.descripcion;
                                }
                                
                                if(gastoEditado.gasto ==""){
                                    validacion.gasto=seleccionada.gasto;
                                }
                                else{
                                    validacion.gasto = gastoEditado.gasto;
                                }

                                if(gastoEditado.categoria ==""){
                                    validacion.categoria=seleccionada.categoria;
                                }
                                else{
                                    validacion.categoria = gastoEditado.categoria;
                                }
                        }
                    }
                    datos_gastos = buscarGastoEditar(datos_gastos);
                    ipcRenderer.send('nombreNuevaCategoria');
                }else{
                    console.log("No categoria");
                }

                
            }else{
                for(const validacion of datos_gastos.gastos_cuentas){
                    if(validacion.email == seleccionada.email
                        && validacion.fecha == seleccionada.fecha
                        && validacion.descripcion == seleccionada.descripcion
                        && validacion.gasto == seleccionada.gasto
                        && validacion.categoria == seleccionada.categoria){
                            validacion.email = gastoEditado.email;
                            validacion.fecha =gastoEditado.fecha;
                            validacion.descripcion = gastoEditado.descripcion;
                            validacion.gasto = gastoEditado.gasto;
                            if(gastoEditado.email ==""){
                                validacion.email=seleccionada.email;
                            }
                            else{
                                validacion.email = gastoEditado.email;
                            }

                            if(gastoEditado.fecha ==""){
                                validacion.fecha=seleccionada.fecha;
                            }
                            else{
                                validacion.fecha = gastoEditado.fecha;
                            }

                            if(gastoEditado.descripcion ==""){
                                validacion.descripcion=seleccionada.descripcion;
                            }
                            else{
                                validacion.descripcion = gastoEditado.descripcion;
                            }
                            
                            if(gastoEditado.gasto ==""){
                                validacion.gasto=seleccionada.gasto;
                            }
                            else{
                                validacion.gasto = gastoEditado.gasto;
                            }
                            validacion.categoria = "general";
                    }
                }
                datos_gastos = buscarGastoEditar(datos_gastos);
                ipcRenderer.send('nombreNuevaCategoria');
            }


        })


        
    }

})