const {ipcRenderer, ipcMain}=require("electron");
const moment = require('moment');
const {
    accesoCuentas,
    buscarCuenta,
    verificarExisteC,
    accesoCategorias,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    accesoGastos,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");

//ÁRBOL DOM
document.addEventListener("DOMContentLoaded",()=>{
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        // evento.preventDefault();
        //alert(`ffffffff ${datos.email}`);
        todo(datos)
        //return datos;
    });

    function sumarDias (fecha){
        fecha.setDate(fecha.getDate()+1);
        return fecha;
    }
    function todo(datos) {
        
        const botonAceptarGasto = document.getElementById("botonAceptarGasto");
        botonAceptarGasto.addEventListener("click",(evento)=>{
            evento.preventDefault();
            let inicioCuenta = {email: datos.email};

            const textoFecha = document.getElementById("textoFecha");
            const entrada1 = textoFecha.value;
            const textoDescripcion = document.getElementById("textoDescripcion");
            const entrada2 = textoDescripcion.value;
            const textoMonto = document.getElementById("textoMonto");
            const entrada3 = textoMonto.value;
            const textoCategoria = document.getElementById("textoCategoria");
            const entrada4 = textoCategoria.value;
            const opcionSi = document.getElementById("opcionSi");
            const opcionNo = document.getElementById("opcionNo");

            let registroGasto = {
                gasto: entrada3,
                descripcion: entrada2,
                fecha: entrada1
            }
            let datos_categorias = accesoCategorias();
            let datos_gastos = accesoGastos();
         let fechacambio = moment(entrada1).format('YYYY/MM/DD');
         //fechacambio = sumarDias(fechacambio);
         console.log(fechacambio);
         let f1 = Date.parse(fechacambio);
         if (isNaN(f1)) {
           console.log("La fecha ingresada no es valida");
         } else {
           //Se verifica el valor del gasto (que sea valor positivo y mayor a 0)
           registroGasto.gasto = parseInt(registroGasto.gasto);
           if (registroGasto.gasto > 0) {
             //let respuesta = await preguntaGasto();
             //Si quiere asociar un gasto a una categoría
             //respuesta.pregunta = respuesta.pregunta.toLowerCase();
             if (opcionSi.checked) {
            //    console.log("respondio si");

               let categoriaConGasto = {nombre: entrada4};
               let existeCategoria = buscarCategoriaEditar(
                 inicioCuenta,
                 datos_categorias,
                 categoriaConGasto
               );
               //Se verifica que existe la categoria en esa cuenta
               if (existeCategoria == 0) {
                 //Se registra el gasto con categoría
                 //ditada
                 registroGasto.fecha = fechacambio;
                 datos_gastos = gastoParaRegistrar1(
                   datos_gastos,
                   inicioCuenta,
                   categoriaConGasto,
                   registroGasto
                 );
                 alert(`Gasto registrado`);
                 ipcRenderer.send('nombreNuevaCategoria');
               } else {
                 console.log("No existe esa categoria en la cuenta");
                 console.log("Intente nuevamente ingresar el gasto");
               }
             } else {
               //Se registra el gasto en general
               registroGasto.fecha = fechacambio;
               datos_gastos = gastoParaRegistrar2(
                 datos_gastos,
                 inicioCuenta,
                 registroGasto
               );
               alert(`Gasto registrado`);
               ipcRenderer.send('nombreNuevaCategoria');
             }
           } else {
            alert(`Valor de gasto no permitido, intente nuevamente`);
           }
         }

    
        })
    }
})