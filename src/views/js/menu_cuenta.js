//Librerias
const {ipcRenderer}=require("electron");
const {
    accesoCuentas,
    buscarCuenta,
    verificarExisteC,
    accesoCategorias,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    accesoGastos,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
  } = require("./js/data");

//ÁRBOL DOM
document.addEventListener("DOMContentLoaded",()=>{

    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        // evento.preventDefault();
        let inicioCuenta = {email: datos.email};
        todo(inicioCuenta);
    })
    
    function todo(inicioCuenta){

        //MOSTRAR LAS CATEGORÍAS
        let datos_categorias = accesoCategorias();
        let arrCategoriasMostrar = [];
        console.log("estoy en mi menu " + inicioCuenta.email);
        for (const validacion of datos_categorias.categorias_cuentas) {
            if (validacion.email == inicioCuenta.email) {
                arrCategoriasMostrar.push(validacion.categoria);
                console.log(validacion.categoria);
            }
        }
        let categorias= document.getElementById("categorias");

        arrCategoriasMostrar.forEach(element => {
            let li = document.createElement('li');
            li.classList.toggle("n_categoria");
            let div = document.createElement('div');
            div.classList.toggle("n_nombre");
            div.innerHTML= `<input type='radio' class='form-check-input n_check' name='registro_cat' id ='${element}' >${element}`;
            li.appendChild(div);
            categorias.appendChild(li);
        });
        
        
        //OPCIONES PARA CATEGORÍA
        const iconoAñadirCategoria = document.getElementById("iconoAñadirCategoria");
        iconoAñadirCategoria.addEventListener("click",(evento)=>{
            evento.preventDefault();
            ipcRenderer.send("añadirLaCategoria");
        })
        const iconoEditarCategoria = document.getElementById("iconoEditarCategoria");
        iconoEditarCategoria.addEventListener("click",(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            arrCategoriasMostrar.forEach(element=>{
                if (document.getElementById(element).checked === true){
                    seleccionada = element;
                }
                
            })
            // alert(`Selección: ${seleccionada}`);

            ipcRenderer.send("editarLaCategoria",seleccionada);
        })
        const iconoEliminarCategoria = document.getElementById("iconoEliminarCategoria");
        iconoEliminarCategoria.addEventListener("click",(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            arrCategoriasMostrar.forEach(element=>{
                if (document.getElementById(element).checked === true){
                    seleccionada = element;
                }
            })
            // alert(`Selección: ${seleccionada}`);

            ipcRenderer.send("eliminarLaCategoria",seleccionada);
        })
        //MOSTRAR GASTOS
        let datos_gastos = accesoGastos();
        let arrGastosMostrar = [];
        console.log("estoy en mi menu " + inicioCuenta.email);
        for (const validacion of datos_gastos.gastos_cuentas) {
            if (validacion.email == inicioCuenta.email) {
                arrGastosMostrar.push(validacion);
                console.log(validacion);
            }
        }
        let contadorG=0;
        arrGastosMostrar.forEach(element =>{
            let tr = document.createElement('tr');

            contadorG++;
            let td1 = document.createElement('td');
            td1.innerHTML = `<input type='radio' name='seleccion_gasto' id ='gasto${contadorG}'>`;
            console.log(contadorG);
            tr.appendChild(td1);

            let td2 = document.createElement('td')
            td2.innerHTML = element.fecha;
            tr.appendChild(td2);

            let td3 = document.createElement('td')
            td3.innerHTML = element.descripcion;
            tr.appendChild(td3);

            let td4 = document.createElement('td')
            td4.innerHTML = element.gasto;
            tr.appendChild(td4);

            let td5 = document.createElement('td')
            td5.innerHTML = element.categoria;
            tr.appendChild(td5);

            tr.classList.toggle("table-primary");

            let tbody= document.getElementById("tBodyGastos");
            
            tbody.appendChild(tr);
            
        })
        //OPCIONES PARA GASTO
        const iconoAñadirGasto = document.getElementById("iconoAñadirGasto");
        iconoAñadirGasto.addEventListener("click",(evento)=>{
            evento.preventDefault();
            ipcRenderer.send("añadirElGasto");
        })
        const iconoEditarGasto = document.getElementById("iconoEditarGasto");
        iconoEditarGasto.addEventListener("click",(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            let contadorG=0;

            arrGastosMostrar.forEach(element=>{
                contadorG++;
                if (document.getElementById(`gasto${contadorG}`).checked === true){
                    seleccionada = element;
                    console.log();
                }
            })
            ipcRenderer.send("editarElGasto",seleccionada);
        })
        const iconoEliminarGasto = document.getElementById("iconoEliminarGasto");
        iconoEliminarGasto.addEventListener("click",(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            let contadorG=0;

            arrGastosMostrar.forEach(element=>{
                contadorG++;
                if (document.getElementById(`gasto${contadorG}`).checked === true){
                    seleccionada = element;
                    console.log();
                }
            })
            ipcRenderer.send("eliminarElGasto",seleccionada);
        })
    
        //OPCIONES VER GASTO
        let select = document.getElementById("f_categoria");
        arrCategoriasMostrar.forEach(element => {
            let cat =document.createElement('option');
            cat.id=element;
            cat.innerHTML=element;
            select.appendChild(cat);
        });

        let arrBuscar = ["historico","mensual","categorico"];
        const aceptarVerGastos = document.getElementById("aceptarVerGastos");
        aceptarVerGastos.addEventListener("click",(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            arrBuscar.forEach(element=>{
                if (document.getElementById(element).checked === true){
                    seleccionada = element;
                }
                
            })
            // alert(`Selección: ${seleccionada}`);
            if(seleccionada == "historico"){
                //nada
                console.log("historico");
                let informacion ={
                    seleccionada:seleccionada,
                    seleccionOp:"nada"
                }
                ipcRenderer.send("mostrarLosGastos",informacion);

            }else if (seleccionada == "mensual"){
                //algo
                let selectM = document.getElementById("n_mes").value;
                let informacion ={
                    seleccionada:seleccionada,
                    seleccionOp:selectM
                }
                ipcRenderer.send("mostrarLosGastos",informacion);
                
            }else if (seleccionada == "categorico"){
                console.log("categorico");
                let selectC = document.getElementById("f_categoria").value;
                      
                let informacion ={
                    seleccionada:seleccionada,
                    seleccionOp:selectC
                }
                ipcRenderer.send("mostrarLosGastos",informacion);
                
            }


            
        })
    }

})