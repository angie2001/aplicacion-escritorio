//Librerias
const {ipcRenderer}=require("electron");
const {
    accesoCuentas,
    buscarCuenta,
    verificarExisteC,
    accesoCategorias,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    accesoGastos,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
  } = require("./js/data");

let inicioCuenta = {email:""};
//ÁRBOL DOM
document.addEventListener("DOMContentLoaded",()=>{
    const boton_aceptar_iniciar_sesion = document.getElementById("boton_aceptar_iniciar_sesion");
    boton_aceptar_iniciar_sesion.addEventListener("click",(evento)=>{
        evento.preventDefault();
        const textoEmailInicio = document.getElementById("textoEmailInicio");
        const entrada1 = textoEmailInicio.value;
        const textoPasswordInicio = document.getElementById("textoPasswordInicio");
        const entrada2 = textoPasswordInicio.value;

        const inicioSesion = {
            email: entrada1,
            password: entrada2
        }
        let datos = accesoCuentas();
        let existe = verificarExisteC(datos, inicioSesion);
        console.log(existe);
        if(existe == true){
            //no se verifica si existe la cuenta
            inicioCuenta.email = inicioSesion.email;
            //let correo = capturaSesion(inicioCuenta);
            ipcRenderer.send("inicioLaSesion", inicioSesion);

            alert(`BIENVENID@ ${entrada1}`);
        }else{
            alert(`DATOS INCORRECTOS`);

        }




    })
})

module.exports={
    inicioCuenta,
};