const {ipcRenderer, ipcMain}=require("electron");
const {
    accesoCuentas,
    buscarCuenta,
    verificarExisteC,
    accesoCategorias,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    accesoGastos,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");
  
document.addEventListener("DOMContentLoaded",()=>{
    let inicioCuenta={email:""};
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        inicioCuenta= {email: datos.email};
        ipcRenderer.on('categoriaSeleccionada',(evento1,seleccionada)=>{
            todo(inicioCuenta, seleccionada);
        })
    })
    
    function todo(inicioCuenta, seleccionada){
        const botonAceptarCambioCategoria = document.getElementById("botonAceptarCambioCategoria");
            botonAceptarCambioCategoria.addEventListener("click",(evento)=>{
                evento.preventDefault();
                const textoCategoria = document.getElementById("textoCategoria");
                const entrada1 = textoCategoria.value;
                
                let datos_categorias = accesoCategorias();
                let datos_gastos = accesoGastos();
                let original = {categoria:seleccionada};
                let nueva_cat = {nombre: entrada1};
                
                let aceptoCambio = buscarCategoriaEditar(
                    inicioCuenta,
                    datos_categorias,
                    nueva_cat
                    );
                    
                if (aceptoCambio == 1) {
                        //Se edita el nombre de la categoría
                    //let nuevoNombreCategoria = {categoria:seleccionada,nombre: entrada1};
                    let nuevoNombreCategoria = {categoria:original.categoria,nombre: nueva_cat.nombre};
                    datos_gastos,
                      (datos_categorias = cambioNombreCategoriaRegistro(
                        datos_gastos,
                        datos_categorias,
                        inicioCuenta,
                        nuevoNombreCategoria
                      ));
                      ipcRenderer.send('nombreNuevaCategoria');
                } else {
                    //alert(`Ya se tiene una categoría con el nuevo nombre`);
                    console.log("Ya se tiene una categoría con el nuevo nombre");
                    console.log("Intente ingresar otro nombre");
                }               
            })
    }
  })