const {ipcRenderer, ipcMain}=require("electron");
const {
    accesoCuentas,
    buscarCuenta,
    verificarExisteC,
    accesoCategorias,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    accesoGastos,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");

  document.addEventListener("DOMContentLoaded",()=>{
    let inicioCuenta={email:""};
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        inicioCuenta= {email: datos.email};
        ipcRenderer.on('categoriaSeleccionada',(evento1,seleccionada)=>{
            todo(inicioCuenta, seleccionada);
            // alert(`cuenta: ${inicioCuenta.email} Selección: ${seleccionada}`);

        })
    })
    function todo (inicioCuenta,seleccionada){
        let catEliminar = document.getElementById('catEliminar');
        catEliminar.textContent=seleccionada; 

        //AHORA
        const botonBorrarCategoria = document.getElementById("botonBorrarCategoria");
        botonBorrarCategoria.addEventListener("click",(evento)=>{
            evento.preventDefault();
            const radioNo = document.getElementById("radioNo");
            const radioSi = document.getElementById("radioSi");
    
            let datos_categorias = accesoCategorias();
            const datos_gastos = accesoGastos();
            const nombredeCategoriaEliminar = {categoria:seleccionada}
            if(radioNo.checked){
                // alert(`no`);
                datos_gastos,
               (datos_categorias = eliminaciondeCategoria2(
                 datos_categorias,
                 datos_gastos,
                 inicioCuenta,
                 nombredeCategoriaEliminar
               ));
               ipcRenderer.send('nombreNuevaCategoria');

            }else if(radioSi.checked){
                // alert(`si`);
                datos_gastos,
               (datos_categorias = eliminaciondeCategoria1(
                 datos_categorias,
                 datos_gastos,
                 inicioCuenta,
                 nombredeCategoriaEliminar
               ));
               ipcRenderer.send('nombreNuevaCategoria');
            }

        })

    }



  })