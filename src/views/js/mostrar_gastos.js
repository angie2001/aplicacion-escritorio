const {ipcRenderer, ipcMain}=require("electron");
const moment = require('moment');
const {
    accesoCuentas,
    buscarCuenta,
    verificarExisteC,
    accesoCategorias,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    accesoGastos,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");

document.addEventListener("DOMContentLoaded",()=>{
    let inicioCuenta={email:""};
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        inicioCuenta= {email: datos.email};
        ipcRenderer.on('vistaSeleccionada',(evento1,informacion)=>{
            
            todo(inicioCuenta, informacion);
        })
    })


    function visualizar(arrMostrar){

            arrMostrar.forEach(element =>{
                let tr = document.createElement('tr');
    
                let td2 = document.createElement('td')
                td2.innerHTML = element.fecha;
                tr.appendChild(td2);
    
                let td3 = document.createElement('td')
                td3.innerHTML = element.descripcion;
                tr.appendChild(td3);
    
                let td4 = document.createElement('td')
                td4.innerHTML = element.gasto;
                tr.appendChild(td4);
    
                let td5 = document.createElement('td')
                td5.innerHTML = element.categoria;
                tr.appendChild(td5);
    
                tr.classList.toggle("table-primary");
    
                let tbody= document.getElementById("tBodyGastos");
                
                tbody.appendChild(tr);
                
            })
    }
    
    function todo(inicioCuenta,informacion) {
        const refrescar = document.getElementById("refrescar");
        refrescar.addEventListener("click",(evento)=>{

            let datos_gastos = accesoGastos();
            if(informacion.seleccionada == "historico"){
                let arrMostrarH = vistaGeneral(datos_gastos,inicioCuenta);
                arrMostrarH.forEach(element =>{
                    let tr = document.createElement('tr');
        
                    let td2 = document.createElement('td')
                    td2.innerHTML = element.fecha;
                    tr.appendChild(td2);
        
                    let td3 = document.createElement('td')
                    td3.innerHTML = element.descripcion;
                    tr.appendChild(td3);
        
                    let td4 = document.createElement('td')
                    td4.innerHTML = element.gasto;
                    tr.appendChild(td4);
        
                    let td5 = document.createElement('td')
                    td5.innerHTML = element.categoria;
                    tr.appendChild(td5);
        
                    tr.classList.toggle("table-primary");
        
                    let tbody= document.getElementById("tBodyGastos");
                    
                    tbody.appendChild(tr);
                    
                })
            }else if(informacion.seleccionada == "categorico"){
                console.log("info seleccionada: "+informacion.seleccionOp);
                let respuestaFiltro = {nombre:informacion.seleccionOp};
                let arrMostrarC = vistaCategoria(datos_gastos,inicioCuenta,respuestaFiltro);
                
                arrMostrarC.forEach(element =>{
                    let tr = document.createElement('tr');
        
                    let td2 = document.createElement('td')
                    td2.innerHTML = element.fecha;
                    tr.appendChild(td2);
        
                    let td3 = document.createElement('td')
                    td3.innerHTML = element.descripcion;
                    tr.appendChild(td3);
        
                    let td4 = document.createElement('td')
                    td4.innerHTML = element.gasto;
                    tr.appendChild(td4);
        
                    let td5 = document.createElement('td')
                    td5.innerHTML = element.categoria;
                    tr.appendChild(td5);
        
                    tr.classList.toggle("table-primary");
        
                    let tbody= document.getElementById("tBodyGastos");
                    
                    tbody.appendChild(tr);
                    
                })
            }else if (informacion.seleccionada == "mensual"){
                console.log("info seleccionada: "+informacion.seleccionOp);
                let respuestaFiltro = {nombre:informacion.seleccionOp};
                let arrMostrarM = vistaMes(datos_gastos,inicioCuenta,respuestaFiltro);
                
                arrMostrarM.forEach(element =>{
                    let tr = document.createElement('tr');
        
                    let td2 = document.createElement('td')
                    td2.innerHTML = element.fecha;
                    tr.appendChild(td2);
        
                    let td3 = document.createElement('td')
                    td3.innerHTML = element.descripcion;
                    tr.appendChild(td3);
        
                    let td4 = document.createElement('td')
                    td4.innerHTML = element.gasto;
                    tr.appendChild(td4);
        
                    let td5 = document.createElement('td')
                    td5.innerHTML = element.categoria;
                    tr.appendChild(td5);
        
                    tr.classList.toggle("table-primary");
        
                    let tbody= document.getElementById("tBodyGastos");
                    
                    tbody.appendChild(tr);
                    
                })
            }
        })
        
    }

})